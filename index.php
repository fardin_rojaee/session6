<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login Form</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body>
<div class="container">
    <div class="btn-container">
        <div class="row">
            <div class="col-md-6 text-right">
                <button type="button" class="btn" data-toggle="modal" data-target="#login-modal">ورود</button>
            </div>
            <div class="col-md-6 text-left ">
                <button type="button" class="btn" data-toggle="modal" data-target="#register-modal">ثبت نام</button>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">

    <div class="modal" id="login-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h6>ورود به صفحه پنل کاربری سایت</h6>
                </div>
                <div class="modal-body">
                    <form action="register-destenition.php" method="post">
                        <input type="hidden" name="action" value="login">
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="نام کاربری" name="username">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="password" placeholder="پسورد" name="password">
                        </div>
                        <div class="captcha-container">
                            <img src="captcha.php"
                                 style="display: block;width: 30%;margin: auto;border: 1px solid aqua">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="captcha" placeholder="کد امنیتی را وارد کنید">
                        </div>


                        <div class="text-center">
                            <input type="submit" value="ورود" class="btn btn-success">
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#">ثبت نام نکرده اید؟</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">

    <div class="modal" id="register-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5>فرم ثبت نام</h5>
                </div>
                <div class="modal-body">
                    <form action="register-destenition.php" method="get" enctype="multipart/form-data">
                        <input type="hidden" name="action" value="register">
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="نام" name="name">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="خانوادگی" name="family">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="tel" placeholder="تلفن" name="phone">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="نام کاربری" name="username">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="password" placeholder="پسورد" name="password">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="password" placeholder=" تکرار پسورد" name="re_password">
                        </div>
                        <div class="form-group col-md-12">
                            <label class="control-label col-md-5" style="float: right">آواتار خود را وارد کنید</label>
                            <input type="file" name="avatar" class="col-md-7" style="float: right">
                        </div>

                        <div class="text-center">
                            <input type="submit" value="ورود" class="btn btn-success">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>