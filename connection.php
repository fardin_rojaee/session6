<?php
/**
 * Created by PhpStorm.
 * User: fardin
 * Date: 1/28/2018
 * Time: 9:51 PM
 */
function connect_db()
{
    $connection = new mysqli("localhost", "root", "", "fridays");
//    $connection = new PDO("mysql:host=localhost;dbname:fridays", "root", "");
    if ($connection->connect_error) {
        echo "error:" . $connection->connect_error;
    } else {
        return $connection;
    }
}