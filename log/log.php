<?php
function getShamsiDate()
{
    include "converter.php";
    $dateTimeGhamari = getdate();
    $year = $dateTimeGhamari['year'];
    $month = $dateTimeGhamari['mon'];
    $day = $dateTimeGhamari['mday'];
    $date = $dateTimeGhamari['hours'] . ":" . $dateTimeGhamari['minutes'] . ":" . $dateTimeGhamari['seconds'];
    $dateTimeShamsiArray = gregorian_to_jalali($year, $month, $day);
    $shamsiYear = $dateTimeShamsiArray[0];
    $shamsiMonths = $dateTimeShamsiArray[1];
    $shamsiDays = $dateTimeShamsiArray[2];
    $shamsi = "$date" . " " . $shamsiYear . "/" . $shamsiMonths . "/" . $shamsiDays;
    return $shamsi;

}

function LogUsersAction($action)
{
    $file = fopen("usersLog.txt", "a+");
    fwrite($file, getShamsiDate() . " " . $action . "\n");
    fclose($file);
}

LogUsersAction("INSERT INTO users (name,family) VALUES ('ali','asghar')");

//$file = "usersLog.txt";
//
//if (!unlink($file)) {
//    echo("error deleting $file");
//} else {
//    echo("deleted $file");
//}